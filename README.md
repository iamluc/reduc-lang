reduc-lang
==========

Exemple de création d'un interpréteur pour un "Domain Specific Language" (DSL).

Permet de lexer, parser et interpréter des règles pour calculer un prix.

Exemples
--------

Lexer

```
./bin/lex.php "SI<SUPERIEUR_A<TOTAL<>, 100>, SOUSTRAIRE<TOTAL<>, 10>, TOTAL<>>"
```

Parser

```
./bin/parse.php "SI<SUPERIEUR_A<TOTAL<>, 100>, SOUSTRAIRE<TOTAL<>, 10>, TOTAL<>>"
```

Interpréter

```
./bin/eval.php "SI<SUPERIEUR_A<TOTAL<>, 100>, SOUSTRAIRE<TOTAL<>, 10>, TOTAL<>>" 132
```

License
-------

[MIT](https://opensource.org/licenses/MIT)
