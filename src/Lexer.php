<?php

require_once __DIR__.'/Token.php';

class Lexer
{
    private $stream;
    private $cursor;

    public function lex($stream)
    {
        $this->stream = $stream;
        $this->cursor = 0;

        $tokens = [];
        while ($token = $this->next()) {
            $tokens[] = $token;
            if (Token::T_EOF === $token->getType()) {
                break;
            }
        }

        return $tokens;
    }

    private function next()
    {
        if ($this->cursor >= strlen($this->stream)) {
            return new Token(Token::T_EOF);
        }

        if ('<' === $this->stream[$this->cursor]) {
            ++$this->cursor;
            return new Token(Token::T_LESS_THAN);
        }

        if ('>' === $this->stream[$this->cursor]) {
            ++$this->cursor;
            return new Token(Token::T_GREATER_THEN);
        }

        if (',' === $this->stream[$this->cursor]) {
            ++$this->cursor;
            return new Token(Token::T_COMMA);
        }

        // Ignore spaces
        if (1 === preg_match('/(\s+)/A', $this->stream, $matches, 0, $this->cursor)) {
            $this->cursor += strlen($matches[1]);

            return $this->next();
        }

        // Integers
        if (1 === preg_match('/(\d+)/A', $this->stream, $matches, 0, $this->cursor)) {
            $this->cursor += strlen($matches[1]);

            return new Token(Token::T_INT, $matches[1]);
        }

        // Functions
        if (1 === preg_match('/([\w_]+)/A', $this->stream, $matches, 0, $this->cursor)) {
            $this->cursor += strlen($matches[1]);

            return new Token(Token::T_FUNC, $matches[1]);
        }

        throw new \LogicException(sprintf('Unable to tokenize the stream near "%s ..." (cursor %d).', substr($this->stream, $this->cursor, 20), $this->cursor));
    }
}
