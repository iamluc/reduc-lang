<?php

require_once __DIR__.'/Nodes.php';

class Interpreter
{
    private $total;

    public function __construct(int $total = 100)
    {
        $this->total = $total;
    }

    public function eval(Node $node)
    {
        if ($node instanceof FuncNode) {
            return $this->evalFuncNode($node);
        }

        if ($node instanceof IntNode) {
            return $this->evalIntNode($node);
        }

        throw new \LogicException(sprintf('Don\'t know how to eval node "%s"', get_class($node)));
    }

    private function evalFuncNode(FuncNode $node)
    {
        $resolvedArgs = [];
        foreach ($node->getArgs() as $arg) {
            $resolvedArgs[] = $this->eval($arg);
        }

        $func = $this->getFunc($node->getName());

        return call_user_func($func, ...$resolvedArgs);
    }

    private function evalIntNode(IntNode $node)
    {
        return $node->getValue();
    }

    private function getFunc(string $name)
    {
        switch ($name) {
            case 'SI':
                return function ($cond, $ifTrue, $ifFalse) {
                      return $cond ? $ifTrue : $ifFalse;
                };

            case 'SUPERIEUR_A':
                return function ($a, $b) {
                    return $a > $b;
                };

            case 'SOUSTRAIRE':
                return function ($a, $b) {
                    return $a - $b;
                };

            case 'TOTAL':
                return function () {
                    return $this->total;
                };
        }

        throw new \LogicException(sprintf('Unknown "%s"', $name));
    }
}
