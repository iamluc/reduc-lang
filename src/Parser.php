<?php

require_once __DIR__.'/Token.php';
require_once __DIR__.'/Nodes.php';

class Parser
{
    private $tokens;

    public function parse(array $tokens): Node
    {
        $this->tokens = $tokens;

        return $this->parseStatement($this->next());
    }

    private function parseStatement(Token $token): Node
    {
        if ($token->is(Token::T_FUNC)) {
            return $this->parseFunc($token);
        }

        if ($token->is(Token::T_INT)) {
            return new IntNode((int) $token->getValue());
        }

        throw new \LogicException(sprintf('Invalid token in statement: %s', $token->getType()));
    }

    private function parseFunc(Token $name): FuncNode
    {
        $this->expect(Token::T_LESS_THAN);

        $args = [];
        while ($token = $this->next()) {
            if ($token->is(Token::T_GREATER_THEN)) {
                break;
            }

            if ($token->is(Token::T_COMMA)) {
                continue;
            }

            $args[] = $this->parseStatement($token);
        }

        return new FuncNode($name->getValue(), $args);
    }

    private function next(): Token
    {
        return array_shift($this->tokens);
    }

    private function expect($expected)
    {
        $next = $this->next()->getType();
        if ($expected !== $next) {
            throw new \LogicException('Got token "%s", expected "%s"', $expected, $next);
        }
    }
}
