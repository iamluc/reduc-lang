<?php

class Token
{
    public const T_EOF = 'eof';
    public const T_FUNC = 'func';
    public const T_INT = 'int';
    public const T_LESS_THAN = 'lessthan';
    public const T_GREATER_THEN = 'greaterthan';
    public const T_COMMA = 'comma';

    private $type;
    private $value;

    public function __construct(string $type, string $value = null)
    {
        $this->type = $type;
        $this->value = $value;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function is(string $type)
    {
        return $this->type === $type;
    }
}
