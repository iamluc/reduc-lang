#!/usr/bin/env php
<?php

require __DIR__.'/../src/Lexer.php';
require __DIR__.'/../src/Parser.php';
require __DIR__.'/../src/Interpreter.php';

$lexer = new Lexer();
$parser = new Parser();
$interpreter = new Interpreter($argv[2] ?? 100);

$ast = $parser->parse($lexer->lex($argv[1]));
$res = $interpreter->eval($ast);

if (function_exists('dump')) {
    dump($res);
} else {
    var_dump($res);
}
