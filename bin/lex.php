#!/usr/bin/env php
<?php

require __DIR__.'/../src/Lexer.php';

$lexer = new Lexer();
$tokens = $lexer->lex($argv[1]);

if (function_exists('dump')) {
    dump($tokens);
} else {
    var_dump($tokens);
}
