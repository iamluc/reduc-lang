#!/usr/bin/env php
<?php

require __DIR__.'/../src/Lexer.php';
require __DIR__.'/../src/Parser.php';

$lexer = new Lexer();
$parser = new Parser();
$ast = $parser->parse($lexer->lex($argv[1]));

if (function_exists('dump')) {
    dump($ast);
} else {
    var_dump($ast);
}
